# config-ovpn

Add multiple open VPNs to network manager at oneshot

## Usage

Run the script on a bash terminal (`bash config`)

Syntax: `bash config [-h|-p]`

### options:
- -h     Print Help.
- -p     Directory containing openvpn configurations, defaults to current directory.
